import java.util

object SortValidator {
  def verifyArraySorted[T](array: Array[T])(implicit ordered: T => Ordered[T]): Unit = {
    for (i <- 1 until array.length) {
      if (array(i) < array(i - 1)) throw new RuntimeException(s"array not sorted at index $i \n ${array}")
    }
  }

}
