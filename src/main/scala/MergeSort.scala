import java.util

import scala.annotation.tailrec
import scala.reflect.{ClassManifest, ClassTag}

/*
size=10000 => numOfMerge=129284 - stack overflow
size=5000 => numOfMerge=60223
 */
object MergeSort {
  def main(args: Array[String]): Unit = {
    val list: List[Int] = SeqGenerator.getRandomList(5000)
    val sortedArray: List[Int] = new MergeSort().mergeSort(list)
    SortValidator.verifyArraySorted(sortedArray.toArray)
    println(util.Arrays.asList(sortedArray))
  }
}

class MergeSort {
  //todo optimal with insertion sort for 7 items -> gain ~20% of performance, don't merge if already sorted
  //todo iterative version
  //todo move items to aux array less often to save some time by switching role of the input and aux array in each of the recursive calls
  //todo count the number of compares with code and on paper and compare
  //todo proof by expansion
  //todo proof by induction
  //todo try to implement inplace merge sort

  def mergeSort[T: ClassTag](ls: List[T])(implicit ordered: T => Ordered[T]): List[T] = {
    @tailrec
    def merge(l: List[T], r: List[T], mergeResult: Array[T], startIndex: Int): Unit = {
      var currIndex = startIndex
      (l, r) match {
        case (Nil, _) =>
          for (currElement <- r) {
            mergeResult(currIndex) = currElement
            currIndex+=1
          }
        case (_, Nil) =>
          for (currElement <- l) {
          mergeResult(currIndex) = currElement
          currIndex+=1
        }
        case (lHead :: lTail, rHead :: rTail) =>
          if (lHead < rHead) {
            mergeResult(currIndex) = lHead
            merge(lTail, r, mergeResult, currIndex + 1)
          }
          else {
            mergeResult(currIndex) = rHead
            merge(rTail, l, mergeResult, currIndex + 1)
          }
      }
    }


    val n = ls.length / 2
    if (n == 0)
      ls
    else {
      val (a, b) = ls splitAt n
      val mergeResult = new Array[T](ls.length)
      merge(mergeSort(a), mergeSort(b), mergeResult, 0)
      List.from(mergeResult)
    }


  }
}
