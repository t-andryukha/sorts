import java.util
import java.util.concurrent.TimeUnit

import ShellSort.shellGapSequence

import scala.util.Random

object SortPerformanceTest {

  /**
   * https://docs.google.com/spreadsheets/d/11tvubivTCkHnadkvmQLq60L4h-TJvSywHWp_083rsNs/edit?usp=sharing
   *
   * @param args
   */
  def main(args: Array[String]): Unit = {
    //    println("shellSortShellSequence")
    //    measurePerformance(new ShellSort(shellGapSequence))
    //    println("shellSortHibbardGapSequence")
    //    measurePerformance(new ShellSort(ShellSort.hibbardGapSequence))
    //    println("shellSortKnuthSequence")
    //    measurePerformance(new ShellSort(ShellSort.knuthGapSequence))
    //    println("mergeSort")
//    measurePerformance((list: List[Ordered) => new MergeSort().mergeSort(list))
  }

  private def measurePerformance(shellSortShellSequence: ShellSort): Unit = {
    var size: Int = 100
    var lastSortTime: Long = 0
    val timeThreshold = TimeUnit.MINUTES.toNanos(1)
    while (lastSortTime < timeThreshold) {
      val startTime = System.nanoTime()
//      sort(shellSortShellSequence, size)
      size *= 2
      val endTime = System.nanoTime()
      lastSortTime = endTime - startTime
      println(s"Array of size $size was sorted in ${TimeUnit.NANOSECONDS.toSeconds(lastSortTime)}")
    }
  }


  private def measurePerformance(sortFunction: List[Ordered[Any]] => List[Ordered[Any]]): Unit = {
    var size: Int = 100
    var lastSortTime: Long = 0
    val timeThreshold = TimeUnit.MINUTES.toNanos(1)
    while (lastSortTime < timeThreshold) {
      val startTime = System.nanoTime()
      sort(sortFunction, size)
      size *= 2
      val endTime = System.nanoTime()
      lastSortTime = endTime - startTime
      println(s"Array of size $size was sorted in ${TimeUnit.NANOSECONDS.toSeconds(lastSortTime)}")
    }
  }

  private def sort(sortFunction: List[Ordered[Any]] => List[Ordered[Any]], size: Int) = {
    val array: Array[Int] = SeqGenerator.getRandomArray(size)
//    sortFunction.apply(List.from(array.toIterable))
    SortValidator.verifyArraySorted(array)
  }

}
