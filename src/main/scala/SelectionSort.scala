import java.util


class SelectionSort {

  def sort(array: Array[Int]) = {
    for (i <- array.indices) {
      var minVal = Integer.MAX_VALUE
      var minIndex = i
      for (j <- i until array.length) {
        if (array(j) < array(minIndex)) {
          minVal = array(j)
          minIndex = j
        }
      }
      array.swap(i, minIndex)
    }
  }

  implicit class RichArray(val arr: Array[Int]) {
    def swap(i: Int, j: Int): Unit = {
      val temp = arr(i)
      arr(i) = arr(j)
      arr(j) = temp
    }
  }

}

object SelectionSort {
  def main(args: Array[String]): Unit = {
    val array: Array[Int] = Array(6, 8, 5, 3, 8, 6, 4, 2, 8, 4, 7, 4, 7, 3, 2, 1)
    new SelectionSort().sort(array)
    println(util.Arrays.toString(array))
  }
}
