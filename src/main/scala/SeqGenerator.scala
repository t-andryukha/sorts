import scala.util.Random

object SeqGenerator {
  def getRandomArray(size: Int): Array[Int] = {
    val array = new Array[Int](size)
    for (i <- array.indices) {
      array(i) = Random.nextInt(100 * array.length)
    }
    array
  }

  def getRandomList(size: Int): List[Int] = {
    List.from(getRandomArray(size))
  }

}
