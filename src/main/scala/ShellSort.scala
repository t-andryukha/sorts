import java.util
import java.util.concurrent.atomic.{AtomicInteger, AtomicLong}

/**
 * https://rosettacode.org/wiki/Sorting_algorithms/Shell_sort#Scala

 * In shell sort, elements at a specific interval are sorted.
 * The interval between the elements is gradually decreased based on the sequence used.
 * the performance of the shell sort depends on the type of sequence used for a given input array.
 */
class ShellSort(val sequenceStrategy: Int => Iterator[Int]) {
  def sort(array: Array[Int]): (AtomicLong, AtomicLong) = {
    val iterator: Iterator[Int] = sequenceStrategy(array.length)
    val numOfSwaps = new AtomicLong(0)
    val numOfComparisons = new AtomicLong(0)
    for (step <- iterator) {
      for (start <- 0 until step) {
        insertionSort(array, step, start, numOfSwaps, numOfComparisons)
      }
    }
    (numOfComparisons, numOfSwaps)
  }

  private def insertionSort(array: Array[Int], step: Int, start: Int, numOfSwaps: AtomicLong, numOfComparisons: AtomicLong): Unit = {
    for (i <- start until array.length by step) {
      var j = i
      while (j >= step && j > 0) {
        val comparison = array(j).compareTo(array(j - step))
        numOfComparisons.addAndGet(1)
        if (comparison < 0) {
          array.swap(j, j - step)
          numOfSwaps.addAndGet(1)
        }
        j -= step
      }
    }
  }


  implicit class RichArray(val arr: Array[Int]) {
    def swap(i: Int, j: Int): Unit = {
      val temp = arr(i)
      arr(i) = arr(j)
      arr(j) = temp
    }
  }

}

object ShellSort {

  def shellGapSequence: Int => Iterator[Int] = (iteratorSize: Int) => {
    new Iterator[Int] {
      private var current = iteratorSize
      override def hasNext: Boolean = current > 1

      override def next(): Int = {
        current = current / 2
        current
      }
    }
  }

  def hibbardGapSequence: Int => Iterator[Int] = (iteratorSize: Int) => {
    new Iterator[Int] {
      private var current = iteratorSize

      override def hasNext: Boolean = current > 1
      override def next(): Int = {
        current = current / 2 - 1
        if (current <= 1) current = 1
        current
      }
    }
  }

  /**
   *
   * (3^k - 1)/2
   * not greater than N/3
   * @return
   */
  def knuthGapSequence: Int => Iterator[Int] = (iteratorSize: Int) => {
    new Iterator[Int] {
      private var nextValue = iteratorSize / 3
      private var _hasNext: Boolean = true

      override def hasNext: Boolean = _hasNext

      override def next(): Int = {
        var curr = nextValue
        nextValue = ((nextValue / 3) * 3 - 1) / 2
        if (curr <= 1) {
          curr = 1
          nextValue = -1
          _hasNext = false
        }
        curr
      }
    }
  }

  def main(args: Array[String]): Unit = {
    //    val array: Array[Int] = Array(6, 8, 5, 3, 8, 6, 4, 2, 8, 4, 7, 4, 7, 3, 2, 1)
    //    val shellSortShellSequence = new ShellSort(shellGapSequence)
    //    val (numOfComparisons, numOfSwaps) = shellSortShellSequence.sort(array)
    //    println(s"For array of size ${array.length} num of comparisons=$numOfComparisons, numOfSwaps=$numOfSwaps")
    //    println(util.Arrays.toString(array))
    //
    //    val array2: Array[Int] = Array(6, 8, 5, 3, 8, 6, 4, 2, 8, 4, 7, 4, 7, 3, 2, 1)
    //    val shellSortHibbardSequence = new ShellSort(hibbardGapSequence)
    //    val (numOfComparisons2, numOfSwaps2) = shellSortHibbardSequence.sort(array2)
    //    println(s"For array of size ${array2.length} num of comparisons=$numOfComparisons2, numOfSwaps=$numOfSwaps2")
    //    println(util.Arrays.toString(array2))

    //    val array3: Array[Int] = Array(6, 8, 5, 3, 8, 6, 4, 2, 8, 4, 7, 4, 7, 3, 2, 1)
    val array3: Array[Int] = Array(129, 27, 179, 331, 369, 357, 621, 609, 758, 800, 782, 930, 867, 1057, 929, 1078, 1054, 1086, 1176, 1142, 1358, 1144, 1523, 1370, 1673, 1371, 1719, 1851, 1873, 1863, 2008, 2541, 2101, 2604, 2224, 3003, 2505, 3132, 2992, 3210, 3446, 3805, 4147, 4071, 4311, 4115, 5020, 5192, 5145, 5214, 5220, 5522, 5278, 5754, 5859, 5927, 5969, 6037, 6140, 6058, 6389, 6120, 6458, 6304, 6578, 6528, 6703, 6681, 6990, 7103, 7068, 7274, 7161, 7312, 7288, 7468, 7381, 7469, 7873, 7740, 7958, 7906, 7996, 7948, 8115, 7978, 8374, 8414, 8426, 8680, 8447, 9094, 8480, 9123, 8490, 9388, 9424, 9427, 9744, 9628)
    val shellSortKnuthSequence = new ShellSort(knuthGapSequence)
    val (numOfComparisons3, numOfSwaps3) = shellSortKnuthSequence.sort(array3)
    println(s"For array of size ${array3.length} num of comparisons=$numOfComparisons3, numOfSwaps=$numOfSwaps3")
    println(util.Arrays.toString(array3))
  }
}
